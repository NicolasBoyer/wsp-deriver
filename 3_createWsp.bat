@echo off
REM Creation du WSP
set lib=bin\lib\lib
set ant=bin\build.xml
set bsfLibList=%lib%\bsf.jar;%lib%\ant-apache-bsf.jar;%lib%\js.jar;%lib%\commons-logging.jar

start /MIN java.exe -classpath "%bsfLibList%;%lib%\xalan\serializer.jar;%lib%\xalan\xsltc.jar;%lib%\xalan\xalan.jar;%lib%\XML\xmltask.jar;%lib%\ant-launcher.jar;%lib%\scenariant.jar;%lib%\ant.jar;%lib%\ant-contrib.jar;%lib%\ant-nodeps.jar;%lib%\ant-trax.jar;" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% mCreateWsp
