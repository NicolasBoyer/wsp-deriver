<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 * LICENCE[[
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1/CeCILL 2.O 
 *
 * The contents of this file are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/ 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the 
 * License. 
 * 
 * The Original Code is kelis.fr code. 
 * 
 * The Initial Developer of the Original Code is 
 * antoine.pourchez@kelis.fr 
 * 
 * Portions created by the Initial Developer are Copyright (C) 2005 
 * the Initial Developer. All Rights Reserved. 
 * 
 * Contributor(s): 
 * 
 * 
 * Alternatively, the contents of this file may be used under the terms of 
 * either of the GNU General Public License Version 2 or later (the "GPL"), 
 * or the GNU Lesser General Public License Version 2.1 or later (the "LGPL"), 
 * or the CeCILL Licence Version 2.0 (http://www.cecill.info/licences.en.html), 
 * in which case the provisions of the GPL, the LGPL or the CeCILL are applicable 
 * instead of those above. If you wish to allow use of your version of this file 
 * only under the terms of either the GPL, the LGPL or the CeCILL, and not to allow 
 * others to use your version of this file under the terms of the MPL, indicate 
 * your decision by deleting the provisions above and replace them with the notice 
 * and other provisions required by the GPL, the LGPL or the CeCILL. If you do not 
 * delete the provisions above, a recipient may use your version of this file under 
 * the terms of any one of the MPL, the GPL, the LGPL or the CeCILL.
 * ]]LICENCE
 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="ISO-8859-1" method="xml"/>
	<xsl:param name="pLogRep"/>
	<xsl:param name="pSilent"/>
	
	<xsl:variable name="app.name">wspDeriver : createWsp</xsl:variable>
	
	<xsl:include href="xUtil.xsl"/>
	  
	<xsl:template match="/wspTransformer">
		<project name="doAutoDiff" default="Main" basedir=".."><!-- Basedir défini dans le build principal : bin -->
			<taskdef classname="com.scenari.ant.SplashSpecialTask" name="splashspecial"/>
			<taskdef classname="com.scenari.ant.XmlDoctype" name="xmldoctype"/>
			<taskdef resource="net/sf/antcontrib/antcontrib.properties"/>
			
			<property name="app.out.path" location="${{app.conf.path}}/{wspOut/@dir}"/>
			<property name="app.in.path" location="${{app.conf.path}}"/>
			<tstamp><format property="timestamp" pattern="yyMMdd-HHmm"/></tstamp>
			<property name="app.fileTrace.path" location="{$pLogRep}/originalUpdate_${{timestamp}}.log"/>
			
			<target name="Main">
				<record action="start" name="${{app.fileTrace.path}}" loglevel="info"/>
				<trycatch property="x.log.analyse.error">
					<try>
						<xsl:apply-templates select="originalFolders"/>
						<echo>---- Les différents updates ont été réalisés avec succés</echo>
					</try>
					<catch>
						<echo>ERREUR : ${x.log.analyse.error}</echo>
					</catch>
				</trycatch>
				<record action="stop" name="${{app.fileTrace.path}}"/>
			</target>
			
		</project>
	</xsl:template>
	
	<xsl:template match="gitSynchro">
		<xsl:for-each select="item">
			<var name="itemPath" value="${{app.in.path}}/{@to}"/>
			<if>
				<available file="${{itemPath}}"/>
				<then>
					<!-- le répertoire existe déjà => update -->
					<xsl:call-template name="tLogMessage">
						<xsl:with-param name="pLevel">info</xsl:with-param>
						<xsl:with-param name="pMessage">Update du répertoire '${app.in.path}/<xsl:value-of select="@to"/>'.</xsl:with-param>
					</xsl:call-template>
					
					<exec executable="git" dir="${{itemPath}}" failonerror="true">
						<arg value="pull"/>
					</exec>
				</then>
				<else>
					<!-- le répertoire n'existe pas => checkOut -->
					<xsl:call-template name="tLogMessage">
						<xsl:with-param name="pLevel">info</xsl:with-param>
						<xsl:with-param name="pMessage">CheckOut du répertoire '${app.in.path}/<xsl:value-of select="@to"/>'.</xsl:with-param>
					</xsl:call-template>
					
					<mkdir dir="${{app.in.path}}"/>
						<exec executable="git" dir="${{app.in.path}}" failonerror="true">
							<arg value="clone"/>
							<arg value="{@from}"/>
							<arg value="{@to}"/>
						</exec>
				</else>
			</if>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>
