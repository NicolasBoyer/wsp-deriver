<?xml version="1.0" encoding="UTF-8"?>
<!--
 * LICENCE[[
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1/CeCILL 2.O 
 *
 * The contents of this file are subject to the Mozilla Public License Version 
 * 1.1 (the "License"); you may not use this file except in compliance with 
 * the License. You may obtain a copy of the License at http://www.mozilla.org/MPL/ 
 * 
 * Software distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License 
 * for the specific language governing rights and limitations under the 
 * License. 
 * 
 * The Original Code is kelis.fr code. 
 * 
 * The Initial Developer of the Original Code is 
 * antoine.pourchez@kelis.fr 
 * 
 * Portions created by the Initial Developer are Copyright (C) 2005 
 * the Initial Developer. All Rights Reserved. 
 * 
 * Contributor(s): 
 * 
 * 
 * Alternatively, the contents of this file may be used under the terms of 
 * either of the GNU General Public License Version 2 or later (the "GPL"), 
 * or the GNU Lesser General Public License Version 2.1 or later (the "LGPL"), 
 * or the CeCILL Licence Version 2.0 (http://www.cecill.info/licences.en.html), 
 * in which case the provisions of the GPL, the LGPL or the CeCILL are applicable 
 * instead of those above. If you wish to allow use of your version of this file 
 * only under the terms of either the GPL, the LGPL or the CeCILL, and not to allow 
 * others to use your version of this file under the terms of the MPL, indicate 
 * your decision by deleting the provisions above and replace them with the notice 
 * and other provisions required by the GPL, the LGPL or the CeCILL. If you do not 
 * delete the provisions above, a recipient may use your version of this file under 
 * the terms of any one of the MPL, the GPL, the LGPL or the CeCILL.
 * ]]LICENCE
 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" method="xml"/>
	<xsl:param name="pAppConfPath"/>
	<!-- résolution des id/refId du fichier de conf -->
	
	<xsl:template match="@*|node()">
		<xsl:choose>
			<xsl:when test="@refId">
				<xsl:variable name="vTagName" select="name()"/>
				<xsl:variable name="vTagRefId" select="@refId"/>
				<xsl:copy>
					<xsl:apply-templates select="@*[name()!='refId']"/>
					<xsl:apply-templates select="//*[name()=$vTagName and @id=$vTagRefId][1]/*"/>
				</xsl:copy>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy>
					<xsl:apply-templates select="@*[name()!='id']|node()"/>
				</xsl:copy>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
		
</xsl:stylesheet>
