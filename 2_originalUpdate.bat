@echo off
REM Update des sources d'origine
set lib=bin\lib\lib
set ant=bin\build.xml
set svnLibList=%lib%\svn\jw-antxtras.jar;%lib%\svn\jw-antxtras-advanced.jar;%lib%\svn\jw-antxtras-antunit.jar;%lib%\svn\ganymed.jar;%lib%\svn\JWare_apis.jar;%lib%\svn\svn4ant.jar;%lib%\svn\svnjavahl.jar;%lib%\svn\svnkit-cli.jar;%lib%\svn\svnkit.jar
set bsfLibList=%lib%\bsf.jar;%lib%\ant-apache-bsf.jar;%lib%\js.jar;%lib%\commons-logging.jar

start /MIN java.exe -classpath "%svnLibList%;%bsfLibList%;%lib%\ant-launcher.jar;%lib%\scenariant.jar;%lib%\ant.jar;%lib%\ant-contrib.jar;%lib%\ant-nodeps.jar;%lib%\ant-trax.jar;" -Xmx150m org.apache.tools.ant.Main -buildfile %ant% mOriginalUpdate
